package com.example.clustereddataservice.exception;

public class DealValidationException extends RuntimeException {
	public DealValidationException(String message) {
		super(message);
	}

}
