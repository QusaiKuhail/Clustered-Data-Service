package com.example.clustereddataservice.exception;

public class DuplicateDealException extends RuntimeException {

	public DuplicateDealException(String message) {
		super(message);
	}

}
